export enum BlogTypes{
    GENERAL = "general",
    SCHEDULED = "scheduled",
}

export enum BlogStatus{
    PUBLISHED = "published",
    UNPUBLISHED = "unpublished"
}