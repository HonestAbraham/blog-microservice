import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Blog } from 'src/entities/blog.entity';
import { env } from 'process';
import { BlogStatus, BlogTypes } from 'src/util/blog_types';
import { BlogUpdateDto } from './dto/blog-update.dto';

@Injectable()
export class BlogRepository {
    @InjectRepository(Blog)
    private blogsRepository: Repository<Blog>;

    constructor(db: DataSource) {
        this.blogsRepository = db.getRepository(Blog);
    }

    delete(id: number) {
        return this.blogsRepository.softDelete(id);
    }

    findAll(skip: number, limit: number) {
        return this.blogsRepository.find({
            where: { blogStatus: BlogStatus.PUBLISHED },
            skip: skip,
            take: limit,
            order: {
                createdDate: "DESC"
            }
        });
    }

    // create(userId: number, title: string, content: string) {
    //     const blog = this.blogsRepository.create({
    //         userId: userId,
    //         title: title,
    //         content: content,
    //         createdDate: new Date()
    //     });
    //     return this.blogsRepository.save(blog);
    // }

    create(userId: number, title: string, content: string, type: BlogTypes, path) {
        let blog;
        if (type == BlogTypes.GENERAL) {

            blog = this.blogsRepository.create({
                userId: userId,
                title: title,
                content: content,
                imageId: path,
                blogStatus: BlogStatus.PUBLISHED,
                createdDate: new Date()
            });
        } else {
            blog = this.blogsRepository.create({
                userId: userId,
                title: title,
                content: content,
                imageId: path,
                blogStatus: BlogStatus.UNPUBLISHED,
                createdDate: new Date()
            });
        }

        return this.blogsRepository.save(blog);

    }

    update(id: number, blogRequestDto: BlogUpdateDto) {
        const result = this.blogsRepository.update({ id }, {
            title: blogRequestDto.title,
            content: blogRequestDto.content,
            createdDate: new Date(),
        });
        return result;
    }

    async getAllBlogsByUser(id: number, skip: number, limit: number) {


        const blogs2 = await this.blogsRepository.find({
            where: {
                userId: id,
                blogStatus: BlogStatus.PUBLISHED
            },
            skip: skip,
            take: limit,
            order: {
                createdDate: "DESC"
            }
        });
        // console.log(blogs2);

        return blogs2;
    }

    getBlogById(id: number) {
        return this.blogsRepository.findOne({
            where: {
                id: id,
                // blogStatus: BlogStatus.PUBLISHED
            }
        });
    }

    async updatePublish() {
        return await this.blogsRepository.update({}, { blogStatus: BlogStatus.PUBLISHED })
    }
}
