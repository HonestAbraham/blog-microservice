import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { BlogRepository } from './blog.repository';
import { BlogUpdateDto } from './dto/blog-update.dto';
import { Cron } from '@nestjs/schedule';


@Injectable()
export class BlogService {


    constructor(
        private blogsRepository: BlogRepository,
    ) { }

    async getAllBlogsByUser(id: number, skip: number, limit: number) {

        return await this.blogsRepository.getAllBlogsByUser(id, skip, limit);
    }


    async findAll(skip: number, limit: number) {
        return this.blogsRepository.findAll(skip, limit);
    }

    async findOne(id: number) {
        const result = await this.blogsRepository.getBlogById(id);
        if (!result) {
            throw new NotFoundException('Blog not found');
        }
        return result;
    }

    async update(isAdmin: boolean, id: number, userId: number, blogUpdateDto: BlogUpdateDto) {
        const blog = await this.findOne(id);
        if (!blog) {
            throw new NotFoundException('Blog not found');
        }

        if (blog.userId !== userId && !isAdmin) {
            throw new UnauthorizedException("You don't have permission to update this blog");
        }

        const result = await this.blogsRepository.update(id, blogUpdateDto);
        if (result.affected == 0) {
            throw new NotFoundException('Blog not found');
        }
        else {
            return this.findOne(id);
        }
    }

    async remove(isAdmin: boolean, id: number, userId: number) {

        const blog = await this.findOne(id);
        if (!blog) {
            throw new NotFoundException('Blog not found');
        }
        if (blog.userId !== userId && !isAdmin) {
            throw new UnauthorizedException("You don't have permission to delete this blog");
        }

        const result = await this.blogsRepository.delete(id);
        if (result.affected == 0) {
            throw new NotFoundException('Blog not found');
        }
        else {
            return "Deleted Blog with id " + id.toString();
        }
    }

    @Cron('0 */2 * * * *	')
    async updatePublishCycle(){
      console.log("Publish Cycle Reached: Updating all unpublished blog ")
      this.blogsRepository.updatePublish()
    }
  
}
