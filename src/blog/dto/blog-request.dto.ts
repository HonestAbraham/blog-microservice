import { ApiProperty } from "@nestjs/swagger";
import { BlogTypes } from "src/util/blog_types";

// export class BlogRequestDto {
//     @ApiProperty({
//         example : "Example Title"
//     })
//     title: string;
//     @ApiProperty({
//         example : "Example Content"
//     })
//     content: string;
// }


export class BlogRequestDto {
    @ApiProperty({
        example : "Example Title"
    })
    title: string;
    @ApiProperty({
        example : "Example Content"
    })
    content: string;

    @ApiProperty({
        example: "01a73d359686401b6e762cb5851d01e2"
    })
    imageId: string;

    @ApiProperty({
        example: BlogTypes.GENERAL
    })
    blogType: BlogTypes;
}