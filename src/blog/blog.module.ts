import { Module, forwardRef } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogRepository } from './blog.repository';
import { PassportModule } from '@nestjs/passport';
import { Blog } from 'src/entities/blog.entity';
import { User } from 'src/entities/User';
import { MulterModule } from '@nestjs/platform-express';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  controllers: [BlogController],
  providers: [BlogService, BlogRepository],
  imports: [TypeOrmModule.forFeature([Blog, User]), ScheduleModule.forRoot(),],
  exports: [BlogService, BlogRepository]
})
export class BlogModule { }
