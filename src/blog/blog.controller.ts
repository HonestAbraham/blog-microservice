import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { BlogService } from './blog.service';
import { BlogUpdateDto } from './dto/blog-update.dto';

@Controller('blog')
export class BlogController {

    constructor(private readonly blogService: BlogService) { }

    @MessagePattern({ cmd: 'get_all_blogs_by_id' })
    getAllBlogsByUser(payload){
        return this.blogService.getAllBlogsByUser(payload.id, payload.skip, payload.limit);
    }

    @MessagePattern({ cmd: 'get_all_blogs'})
    getAllBlogs(payload){
        console.log("test")
        const blogs = this.blogService.findAll(payload.skip, payload.limit) 
        console.log("test:" + blogs)
        return blogs;
    }

    @MessagePattern({ cmd: 'update_blog'})
    updateBlog(payload){
        return this.blogService.update(payload.isAdmin, payload.id, payload.userId, payload.blogUpdateDto)
    }

    @MessagePattern({ cmd: 'remove_blog'})
    remove(payload){
        return this.blogService.remove(payload.isAdmin, payload.id, payload.userId)
    }
}
