import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BlogController } from './blog/blog.controller';
import { BlogService } from './blog/blog.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule, ConfigService } from '@nestjs/config';
import { User } from './entities/User';
import { Blog } from './entities/blog.entity';
import { BlogModule } from './blog/blog.module';

@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forFeature([User, Blog]),
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    useFactory: (configService: ConfigService) => ({
      type: 'mysql',
      host: configService.get<string>('DATABASE_HOST'),
      port: parseInt(configService.get<string>('DATABASE_PORT')),
      username: configService.get<string>('DATABASE_USER'),
      password: configService.get<string>('DATABASE_PASS'),
      database: configService.get<string>('DATABASE_NAME'),
      entities: [User, Blog],
      synchronize: false,
      logging: ["error", "query"]
    }),
    inject: [ConfigService],
  }),
  BlogModule,
  ],
  controllers: [AppController, BlogController],
  providers: [AppService, BlogService],
})
export class AppModule { }
