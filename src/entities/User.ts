import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Blog } from "./blog.entity";
import { UserRole } from "src/util/UserRole";




@Entity({ name: "users" })
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    email: string;

    @Column()
    password: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({ unique: true })
    username: string

    @OneToMany(() => Blog, (blog) => blog.user)
    blogs: Blog[]

    @Column({ type: 'enum', enum: UserRole, default: UserRole.Member })
    role: UserRole;

    @DeleteDateColumn({ nullable: true })
    deletedAt?: Date;
}